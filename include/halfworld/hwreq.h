#ifndef HWREQ_H
#define HWREQ_H

#include <limits.h>
#include <stddef.h>
#include <stdint.h>

#ifndef UINT8_MAX
#error uint8_t is required, sorry!
#endif

#ifndef UINT16_MAX
#error uint16_t is required, sorry!
#endif

typedef uint8_t hwuchar;
typedef uint16_t hwuint;
#endif
